// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "UObject/ConstructorHelpers.h"

// Sets default values
ALevelActor::ALevelActor()
{
	Height = 5;
	Width = 5;
	
	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	Floor->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Floor->AddLocalOffset(FVector(0.f, 0.f, -50.f));

	ConstructorHelpers::FObjectFinder<UStaticMesh> FloorMesh(TEXT("StaticMesh'/Engine/BasicShapes/Plane.Plane'"));
	if (FloorMesh.Succeeded())
	{
		Floor->SetStaticMesh(FloorMesh.Object);
		Floor->SetWorldScale3D(FVector(Height, Width, 1.f));
	}

	ConstructorHelpers::FObjectFinder<UMaterialInterface> FloorMaterial(TEXT("Material'/Game/Materials/M_Floor.M_Floor'"));
	if (FloorMaterial.Succeeded())
	{
		Floor->SetMaterial(0, FloorMaterial.Object);
	}

	FVector LevelOrigin = Floor->GetComponentLocation();
	FVector BoundsOrigin[4] = 
	{
		{ 100.f * int32(LevelOrigin.X / 100.f + (Height / 2 + 1)), 0.f, 0.f },
		{ 100.f * int32(LevelOrigin.X / 100.f - (Height / 2 + 1)), 0.f, 0.f },
		{ 0.f, 100.f * int32(LevelOrigin.Y / 100.f - (Width / 2 + 1)), 0.f },
		{ 0.f, 100.f * int32(LevelOrigin.Y / 100.f + (Width / 2 + 1)), 0.f }
	};

	for (int i = 0; i < 4; ++i)
	{
		Bounds.Add(CreateDefaultSubobject<UBoxComponent>(FName(*FString::FromInt(i))));
		Bounds[i]->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		Bounds[i]->SetCollisionObjectType(ECC_GameTraceChannel1);
		Bounds[i]->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECR_Block);
		Bounds[i]->SetBoxExtent(FVector(50.f, 50.f, 50.f));
		Bounds[i]->SetWorldScale3D(FVector((i < 2 ? Width : Height), 1.f, 1.f ));
		Bounds[i]->SetWorldLocationAndRotationNoPhysics(BoundsOrigin[i], FRotator(0.f, (i < 2 ? 90.f : 0.f), 0.f));
		Bounds[i]->bHiddenInGame = false;
	}
}