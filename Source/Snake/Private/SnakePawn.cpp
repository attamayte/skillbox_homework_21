// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakePawn.h"
#include "SnakeActor.h"
#include "LevelActor.h"
#include "FoodActor.h"
#include "Engine.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "Camera/CameraComponent.h"


// Sets default values
ASnakePawn::ASnakePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetWorldLocationAndRotation(FVector(0.f, 0.f, 1000.f), FRotator(-90.f, 0.f, 0.f));

	CurrentDirection = NewDirection = ESnakeDirection::None;
}


// Called when the game starts or when spawned
void ASnakePawn::BeginPlay()
{
	Super::BeginPlay();

	if (auto World = GetWorld())
	{
		Snake = World->SpawnActor<ASnakeActor>(SnakeActorClass);
		Level = World->SpawnActor<ALevelActor>(LevelActorClass);

		FreePlacesOnMap.Init(true, Level->Width * Level->Height);
		FreePlacesOnMap[MakeMapIndexFromLocation(Snake->GetActorLocation())] = false;

		Food = World->SpawnActor<AFoodActor>(FoodActorClass, GetFoodSpawnLocation(), FRotator());
		TotalFood = (Level->Height * Level->Width) - 1;
		
		OnSnakeInitialized.Broadcast(TotalFood);
		
		SetActorTickInterval(.5f);
	}
	
}

void ASnakePawn::UpdateMap()
{
	for (auto& Place : FreePlacesOnMap) Place = true;

	FreePlacesOnMap[MakeMapIndexFromLocation(Snake->GetActorLocation())] = false;
	for (const auto TailElem : Snake->GetTail())
	{
		FreePlacesOnMap[MakeMapIndexFromLocation(TailElem->GetComponentLocation())] = false;
	}
	FreePlacesOnMap[MakeMapIndexFromLocation(Food->GetActorLocation())] = false;
}


FVector ASnakePawn::MakeLocationFromMapIndex(int32 Index)
{
    float X = 100.f * (Index / Level->Width - Level->Height / 2);
    float Y = 100.f * (Index % Level->Width - Level->Width / 2);
	return {X, Y, 0.f};
}


int32 ASnakePawn::MakeMapIndexFromLocation(FVector Location)
{
	const int32 X = int32(Location.X / 100.f + Level->Height / 2);
	const int32 Y = int32(Location.Y / 100.f + Level->Width / 2);
	return X * Level->Width + Y;
}


int32 ASnakePawn::FindFreeMapIndex()
{
	int32 
		MapSize 	= FreePlacesOnMap.Num(), 
		MaxIndex 	= FreePlacesOnMap.Num() - 1,
    	Index 		= FMath::RandRange(0, MaxIndex);

    if (FreePlacesOnMap[Index] == false)
    {
		int32 Index2 = Index;
        while (!(FreePlacesOnMap[Index] | FreePlacesOnMap[Index2]))
        {
			Index = MaxIndex - (MaxIndex - Index + 1) % MapSize;
			Index2 = (Index2 + 1) % MapSize;
			if (FMath::Abs(Index - Index2) < 2) return 0;
        }

		if (FreePlacesOnMap[Index2]) Index = Index2;
    }
	return Index;
}


FVector ASnakePawn::GetFoodSpawnLocation()
{
	return MakeLocationFromMapIndex(FindFreeMapIndex());
}


void ASnakePawn::DrawDebugMap()
{
	FString Line;
	for (int i = 0; i < Level->Height; ++i) 
	{
		for (int j = 0; j < Level->Width; ++j)
		{
			Line += FString::FromInt(FreePlacesOnMap[i * Level->Width + j]);
		}
		GEngine->AddOnScreenDebugMessage(-1, 0.5f, FColor::Green, Line);
		Line.Reset();
	}
}

// Called every frame
void ASnakePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (NewDirection != ESnakeDirection::None)
	{
		CurrentDirection = NewDirection;
		NewDirection = ESnakeDirection::None;
	}

	ProbeNextMove();
	if (bCanMove) Snake->Move(MoveVector[static_cast<int>(CurrentDirection)] * 100.f);

	UpdateMap();
	DrawDebugMap();
}


void ASnakePawn::OnUpPressed()
{
	if (NewDirection == ESnakeDirection::None && CurrentDirection != ESnakeDirection::Down)
	{
		NewDirection = ESnakeDirection::Up;
	}
}


void ASnakePawn::OnDownPressed()
{
	if (NewDirection == ESnakeDirection::None && CurrentDirection != ESnakeDirection::Up)
	{
		NewDirection = ESnakeDirection::Down;
	}
}


void ASnakePawn::OnLeftPressed()
{
	if (NewDirection == ESnakeDirection::None && CurrentDirection != ESnakeDirection::Right)
	{
		NewDirection = ESnakeDirection::Left;
	}
}


void ASnakePawn::OnRightPressed()
{
	if (NewDirection == ESnakeDirection::None && CurrentDirection != ESnakeDirection::Left)
	{
		NewDirection = ESnakeDirection::Right;
	}
}


// Called to bind functionality to input
void ASnakePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Up", IE_Pressed, this, &ASnakePawn::OnUpPressed);
	PlayerInputComponent->BindAction("Down", IE_Pressed, this, &ASnakePawn::OnDownPressed);
	PlayerInputComponent->BindAction("Left", IE_Pressed, this, &ASnakePawn::OnLeftPressed);
	PlayerInputComponent->BindAction("Right", IE_Pressed, this, &ASnakePawn::OnRightPressed);
}


void ASnakePawn::Consume(AActor* ConsumedActor)
{
	if (Cast<AFoodActor>(ConsumedActor))
	{
		Snake->Grow();
		OnFoodConsumed.Broadcast(++FoodEaten);
		if (FoodEaten >= TotalFood)
		{
			OnSnakeGameOver.Broadcast(ESnakeGameOverReason::AteAllFood);
			SetActorTickEnabled(false);
		}
		Food->SetActorLocation(GetFoodSpawnLocation());
		bCanMove = true;
	}
	else if (Cast<ASnakeActor>(ConsumedActor))
	{
		SetActorTickEnabled(false);
		OnSnakeGameOver.Broadcast(ESnakeGameOverReason::AteItself);
	}
	else if (Cast<ALevelActor>(ConsumedActor))
	{
		SetActorTickEnabled(false);
		OnSnakeGameOver.Broadcast(ESnakeGameOverReason::LeftLevel);
	}
	else
	{
		// Strange matter
	}
}


void ASnakePawn::ProbeNextMove()
{
	FHitResult TraceResult;
	FCollisionQueryParams TraceParams;
	TraceParams.AddIgnoredComponent(Snake->GetHead());

	FVector TraceStart = Snake->GetActorLocation();
	FVector TraceEnd = Snake->GetActorLocation() + MoveVector[static_cast<int32>(CurrentDirection)] * 100.f; 

	GetWorld()->LineTraceSingleByChannel(TraceResult, TraceStart, TraceEnd, ECC_GameTraceChannel2, TraceParams);
	
	if (TraceResult.bBlockingHit)
	{
		bCanMove = false;
		Consume(TraceResult.GetActor());
	}
	else
	{
		bCanMove = true;
	}
}