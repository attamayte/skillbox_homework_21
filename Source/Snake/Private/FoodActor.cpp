// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodActor.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AFoodActor::AFoodActor()
{
	Food = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Food"));
}

// Called when the game starts or when spawned
void AFoodActor::BeginPlay()
{
	Super::BeginPlay();

	if (FoodMesh) Food->SetStaticMesh(FoodMesh);
	if (FoodMaterial) Food->SetMaterial(0, FoodMaterial);
}
