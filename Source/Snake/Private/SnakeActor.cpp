// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActor.h"
#include "Components/StaticMeshComponent.h"
#include "Engine.h"



// CONSTRUCTOR
ASnakeActor::ASnakeActor()
{
	Head = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Head"));
}

// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	if (HeadMesh) Head->SetStaticMesh(HeadMesh);
	if (HeadMaterial) Head->SetMaterial(0, HeadMaterial);
}


void ASnakeActor::Move(const FVector& Offset)
{
	FVector NextLocation = GetActorLocation(), PrevLocation;
    AddActorWorldOffset(Offset);

    for (int i = 0; i < Tail.Num(); ++i)
    {
        PrevLocation = Tail[i]->GetComponentLocation();
        Tail[i]->SetWorldLocation(NextLocation);
        NextLocation = PrevLocation;
    }
}

void ASnakeActor::Grow()
{
	auto i = Tail.Add(NewObject<UStaticMeshComponent>(this, NAME_None));
	Tail[i]->RegisterComponent();
	Tail[i]->SetStaticMesh(TailMesh);
	Tail[i]->SetMaterial(0, TailMaterial);
}