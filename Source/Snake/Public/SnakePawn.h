// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakePawn.generated.h"

UENUM(Blueprintable, BlueprintType)
enum class ESnakeDirection : uint8
{
	Up,
	Down,
	Left,
	Right,
	None
};

UENUM(Blueprintable, BlueprintType)
enum class ESnakeGameOverReason : uint8
{
	AteAllFood,
	AteItself,
	LeftLevel
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFoodConsumedSignature, int32, FoodConsumed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSnakeGameOverSignature, ESnakeGameOverReason, Reason);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSnakeInitializedSignature, int32, TotalFood);

UCLASS()
class SNAKE_API ASnakePawn : public APawn
{
	GENERATED_BODY()

	const FVector MoveVector[5]
	{
		{ 1.f,  0.f,  0.f},
		{-1.f,  0.f,  0.f},
		{ 0.f, -1.f,  0.f},
		{ 0.f,  1.f,  0.f},
		{ 0.f,  0.f,  0.f}
	};
	
public:
	// Sets default values for this pawn's properties
	ASnakePawn();

	UPROPERTY(Category = "Types", EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class ASnakeActor> SnakeActorClass;

	UPROPERTY(Category = "Types", EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class AFoodActor> FoodActorClass;

	UPROPERTY(Category = "Types", EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<class ALevelActor> LevelActorClass;

	UPROPERTY(Category = "Gameplay", BlueprintAssignable)
	FFoodConsumedSignature OnFoodConsumed;

	UPROPERTY(Category = "Gameplay", BlueprintAssignable)
	FSnakeGameOverSignature OnSnakeGameOver;

	UPROPERTY(Category = "Gameplay", BlueprintAssignable)
	FSnakeInitializedSignature OnSnakeInitialized;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	TArray<bool> FreePlacesOnMap;

	FTimerHandle SnakeTimer;

	UPROPERTY(Category = "Gameplay", VisibleInstanceOnly, BlueprintReadOnly)
    int32 TotalFood;

	UPROPERTY(Category = "Gameplay", VisibleInstanceOnly, BlueprintReadOnly)
    int32 FoodEaten;
	
	bool bCanMove;

	void OnUpPressed();
	void OnDownPressed();
	void OnLeftPressed();
	void OnRightPressed();

	void ProbeNextMove();
	void UpdateMap();

	int32 FindFreeMapIndex();
	int32 MakeMapIndexFromLocation(FVector Location);
	FVector MakeLocationFromMapIndex(int32 Index);

	void Consume(AActor* ConsumedActor);
	
	FVector GetFoodSpawnLocation();
	
	void DrawDebugMap();

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
	class UCameraComponent* Camera;

	UPROPERTY(Category = "Actors", VisibleAnywhere, BlueprintReadOnly)
    class ASnakeActor* Snake;

	UPROPERTY(Category = "Actors", VisibleAnywhere, BlueprintReadOnly)
    class AFoodActor* Food;

	UPROPERTY(Category = "Actors", VisibleAnywhere, BlueprintReadOnly)
    class ALevelActor* Level;

	UPROPERTY(Category = "Gameplay", VisibleAnywhere, BlueprintReadOnly)
    ESnakeDirection CurrentDirection;
	
	UPROPERTY(Category = "Gameplay", VisibleAnywhere, BlueprintReadOnly)
    ESnakeDirection NewDirection;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
