// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelActor.generated.h"

UCLASS()
class SNAKE_API ALevelActor : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ALevelActor();

	UPROPERTY(Category = Level, EditDefaultsOnly, BlueprintReadOnly)
    int32 Width;

	UPROPERTY(Category = Level, EditDefaultsOnly, BlueprintReadOnly)
    int32 Height;

protected:

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
    class UStaticMeshComponent* Floor;

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
    TArray<class UBoxComponent*> Bounds;

};
