// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodActor.generated.h"

UCLASS()
class SNAKE_API AFoodActor : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AFoodActor();

	UPROPERTY(Category = Game, EditDefaultsOnly, BlueprintReadOnly)
	class UStaticMesh* FoodMesh;

	UPROPERTY(Category = Game, EditDefaultsOnly, BlueprintReadOnly)
	class UMaterialInterface* FoodMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Category = Game, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* Food;

};
