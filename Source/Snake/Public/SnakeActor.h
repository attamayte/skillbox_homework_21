// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActor.generated.h"

UCLASS()
class SNAKE_API ASnakeActor : public AActor
{
	GENERATED_BODY()


public:	
	// Sets default values for this actor's properties
	ASnakeActor();

	UPROPERTY(Category = "Mesh", EditDefaultsOnly, BlueprintReadOnly)
	class UStaticMesh* HeadMesh;

	UPROPERTY(Category = "Material", EditDefaultsOnly, BlueprintReadOnly)
	class UMaterialInterface* HeadMaterial;

	UPROPERTY(Category = "Mesh", EditDefaultsOnly, BlueprintReadOnly)
	class UStaticMesh* TailMesh;

	UPROPERTY(Category = "Material", EditDefaultsOnly, BlueprintReadOnly)
	class UMaterialInterface* TailMaterial;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
	class UStaticMeshComponent* Head;

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
    TArray<UStaticMeshComponent*> Tail;
	

public:	

	UFUNCTION(Category = "Gameplay", BlueprintCallable)
	void Move(const FVector& Offset);

	UFUNCTION(Category = "Gameplay", BlueprintCallable)
	void Grow();	

	UFUNCTION(Category = "Components", BlueprintCallable)
	UStaticMeshComponent* GetHead() { return Head; }
	
	UFUNCTION(Category = "Components", BlueprintCallable)
	TArray<UStaticMeshComponent*>& GetTail() { return Tail; }
	
};
